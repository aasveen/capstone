# CAPSTONE PROGRESS

# Meeting Minutes - 26th September,2019


* What was done last week?
	* NA
* What will you do next week
	* Implement networking layer and Category screen to get categories from Firebase database.
* Any Feedbacks
	* language of parental help screen should be the secondary language.

# Meeting Minutes - 10th October,2019


* What was done last week?
	* Implemented networking layer and Category screen to get categories from Firebase database.
* What will you do next week
	* Implment Theme manager  
	* Images for category
	* Screen to pick primary and secondary language screen
* Any Feedbacks
	* NA
	
# Meeting Minutes - 17th October,2019
MISSED 

# Meeting Minutes - 24th October,2019 
* What was done last week?
	* Implemented Theme manager
	* Screen to pick primary and secondary language screen(Partially)
	* Show Images for each category
* What will you do next week
	* Complete screen to pick primary and secondary language screen
	* Show images under each category
* Any Feedbacks
	* NA
	
# Meeting Minutes - 14th October,2019 
* What was done last week?
	* show pick primary and secondary language screen on launch
	* show pick primary and secondary language screen in settings area
	* completed code to upload images to firebase cloud
	* completed code to upload images related data to firebase database
	* uploaded few images for vegetable category on firebase cloud and database
	* Show images under vegetable category in app 
* What will you do next week
	* Add data for more images to firebase cloud and database
	* create a class diagram to show architecture of app so far
	* Show images under each category in app 
* Any Feedbacks
	* NA
	
# Meeting Minutes - 9nd April,2020 
* What was done last week?
	* Demo to team members on Capstone so far.
	* Investigation into how to implement Camera in my app using UIImagePicker class(iOS)
	* Sample code for working camera
* What will you do next week
	*  Integrate findings and camera code into Capstone app
* Any Feedbacks
	* NA
* Open Questions
	* Progress Report for Committee members
	* Can I send app flow video?
	
# Meeting Minutes - 16nd April,2020 
* What was done last week?
	* Integrated findings and camera code into Capstone app
* What will you do next week
	* Database Quiz Screen - Start
	* Game Manager - Start
	* Reusable result View - Start
* Any Feedbacks
	* NA
* TODO
	* Progress Report for Committee members
	
# Meeting Minutes - 30th April,2020 
* What was done last week?
	* Random Category added to practice options
	* Database Quiz Screen - In progress
	* Game Manager - In progress 
		* Show 10 questions based on category selected including random.
		* Showing options of different ways to answer the question.[Camera, Library, Database pictures]
		* Opening Camera, Library, Database Screen.
		* Implemented "pass" the question functionality.
		* Showing Database screen with quiz question and answer options
	* Reusable result View - In progress
* What will you do next week
	* Database Quiz Screen - Complete
	* Game Manager - Complete 
		* Score managment
		* Show appropriate result view for each answered question
		* Game Flow
		* Show final results when game is over
	* Reusable result View - Integrate in both Camera quiz and database screen
* Any Feedbacks
	* NA
* TODO
	* Progress Report for Committee members
	
# Meeting Minutes - 4th June,2020 
	
## What was done last week?
	* Game flow revisited and completed
	* Game introductuon page added
	* Nav button added to switch between different ways to answer the question, source - camera, library and database
	* Activity indicator added while game is loading
	* Quitting the game "action alert" implemented
		    * cancel - will do nothing
			* yes - will let the user quit from the game and show final scores
			
## What will you do next week
	* Integrate ML models
	* Complete ML manager logic 
	* find a way to show the model efficiency?
## Any Feedbacks
	* NA
## TODO
	* Progress Report for Committee members
	
# Meeting Minutes - 2th July,2020 
	
## What was done last week?
	* Integrate ML model - MobileNetV2
	* Complete ML manager logic 
	* Implement ML architecture that is easy to scale
	* Data request to following sources: 
		* http://tdil-dc.in/index.php?lang=en (National Language Technology Proloferation and Deployment Center)
		* https://data.ldcil.org/speech/punjabi-raw-speech-corpus (LINGUISTIC DATA CONSORTIUM FOR INDIAN LANGUAGES)
	* I have received audio files for 500 words in hindi, audio for punjabi words is in progress
	* Implemented Get Help Screen(In progress)
			
## What will you do next week
	* Complete Get Help Screen
	* Upload audio files to firebase
	* Completet History Screen
## Any Feedbacks
	* NA
## TODO
	* Progress Report for Committee members
	
# Meeting Minutes - 6th August,2020 
	
## What was done last month?
	* Completed Get Help Screen - Upload user images to cloud, completed the flow
	* Upload audio files to firebase - In Progress
	* Completet History Screen - complete
			
## What will you do next month
	* Upload audio files to firebase - complete
	* location based notification - complete
	* upload app on testflight for beta testing - complete
	* start documentation 
## Any Feedbacks
	* NA
## TODO
	* Progress Report and app flow video for Committee members
	




